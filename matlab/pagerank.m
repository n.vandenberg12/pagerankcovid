% Connectivity matrix

function vector = pagerank(adj_matrix, iterations, damp)
    n = size(adj_matrix, 2);
    r = ones(n, 1) / n;
    
    totals = sum(adj_matrix, 1);
    empties = zeros(1, n);
    empties(totals == 0) = 1;
    D = diag(totals + empties);
    part1 = damp .* adj_matrix * inv(D);
    part2 = ((1 - damp) / n) .* ones(n, n);
    part3 = (damp / n) * (ones(n, 1) * empties);
    iterator_matrix = part1 + part2 + part3;
    for i = 1:iterations
        r = iterator_matrix * r;
    end
    vector = r;
end