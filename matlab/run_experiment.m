function plot = run_experiment()
    
    names = ["algorithm-archive", "nos", "holtkamp", "wikipedia", ...
        "mitchellfaas", "medium"]

    websites = ["https://www.algorithm-archive.org" ...
                "https://nos.nl/" ...
                "https://www.patisserieholtkamp.nl/" ...
                "https://www.wikipedia.org/" ...
                "https://mitchellfaas.com/" ...
                "https://medium.com/"]
    
    for i = 1:6
        url = sprintf('%s', websites(i));
        [dict, adj_matrix] = surfer(url, 500);
        vector = pagerank(adj_matrix, 1000, 0.85);
        
        title(append('adjacency matrix ', url))
        saveas(gcf, append('adj_', names(i), '.jpg'))
        
        clf;
        plot = histogram(vector);
        title(url);
        xlabel('PageRanks');
        ylabel('number of websites');
        saveas(gcf, append('hist_', names(i), '.jpg'))
    end
end