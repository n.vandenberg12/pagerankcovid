using HTTP
using Gumbo
using AbstractTrees
using Sockets
using SparseArrays
using URIs
using JLD
using Dates

include("webmatrix.jl")

#define our crawler     

mutable struct Crawler
    urlsVisited :: Array{String}
    urlsToCrawl :: Array{String}
    network :: Dict{String, Set{String}}
    
    function Crawler(starturls::Array{String})           
        return new(String[],
                   starturls,
                   Dict{String, Array{String}}())
    end

    function Crawler(urlsVisited, urlsToCrawl, network)
        return new(urlsVisited, urlsToCrawl, network)
    end

    function Crawler(starturl::String)
        return Crawler([starturl])
    end    
end

function crawl(crawler::Crawler, num_iterations::Integer=10, verbose=true)

    for i in 1 : num_iterations
        
        if isempty(crawler.urlsToCrawl)
            return
        end

        url = pop!(crawler.urlsToCrawl)
        
        # if we've already crawled the url, then we skip
        if !(haskey(crawler.network, url))

            push!(crawler.urlsVisited, url)

            if verbose == true
                println("requesting $url")
            end
            
            response = pingUrl(url)

            # if there's no response, then we don't do anything currently

            links = Set{String}()
            
            if response != nothing
                
                doc = parseResponse(response)

                if doc != nothing
                    links = extractLinks(doc, url)
                end
            end

            # update the crawler
            append!(crawler.urlsToCrawl, links)
            crawler.network[url] = links
            
        else
            num_iterations += 1
        end
    end
end

function printNetwork(crawler::Crawler)    
    for (url, connected) in crawler.network
        println(url)
        for other in connected
            println("\t $other")
        end
    end
end

function toConnectivityMatrix(network)
    toIndex = Dict()
    toUrl = []

    rowpts = []
    colpts = []
    vals = []

    for url in keys(network)

        # if we haven't already seen the url, add it to the translation table
        if (!haskey(toIndex, url))
            push!(toUrl, url)
            toIndex[url] = length(toUrl)
        end

        index1 = toIndex[url]
        
        for linked in network[url]

            if (!haskey(toIndex, linked))
                push!(toUrl, linked)
                toIndex[linked] = length(toUrl)
            end

            index2 = toIndex[linked]
            
            push!(rowpts, index1)
            push!(colpts, index2)
            push!(vals, true)
        end
    end

    # bit funky but it works
    matrix = convert(SparseMatrixCSC{Int64, Int64}, sparse(rowpts, colpts, vals))

    # make the matrix square
    for _ in 1:(size(matrix, 2) - size(matrix, 1))
        matrix = [matrix ; spzeros(1, size(matrix, 2))]
    end
    
    return matrix, toIndex, toUrl
    
end

function pingUrl(url::String)
    try
        response = HTTP.get(url)
        return response
    catch err
        if isa(err, Sockets.DNSError)
            return nothing
        elseif isa(err, ArgumentError)
            return nothing
        elseif isa(err, HTTP.ExceptionRequest.StatusError)
            return nothing
        else
            rethrow()
        end
    end
end

function parseResponse(response::HTTP.Response)
    try
        res = String(response.body)
        doc = parsehtml(res)
        return doc
    catch
        return nothing
    end
end

function extractLinks(doc::Gumbo.HTMLDocument, url::String)
    # get links array
    links = Set{String}()
    link_key = "href"
    fail_key = "#"
    for elem in PostOrderDFS(doc.root)
        if typeof(elem) == Gumbo.HTMLElement{:a}
            link = get(elem.attributes, link_key, fail_key)
            
            if link != "#"
                # parse out absolute links and join absolute links
                uriLink = URI(link)
                uriURL = URI(url)

                if isempty(uriLink.scheme) # this is a relative link
                    try
                        uriLink = absuri(uriLink, uriURL) # make absolute
                        push!(links, string(uriLink))
                    catch (err)
                        
                    end
                    #println("url found: $uriLink")
                    
                else
                    if isvalid(uriLink) && !isempty(uriLink.uri)
                        #println("url found: $uriLink")
                        push!(links, uriLink.uri)
                    end
                end
            end
        end
    end
    return links
end

function resetCrawler(crawler::Crawler)
        # this function just resets all the stuff and clears it
        empty!(crawler.urlsVisited)
        empty!(crawler.urlsToCrawl)
        crawler.network = Dict()
        return crawler
end

function setupCrawler(urls)
    return Crawler(urls)
end

function beginCrawl(urls, location::String, num_iterations::Integer=10)
    crawler = setupCrawler(urls)
    crawl(crawler, num_iterations, true)
    matrix, toIndex, toURL = toConnectivityMatrix(crawler.network)
    save(location, "webMatrix", WebMatrix(toIndex, toURL, matrix))
end
