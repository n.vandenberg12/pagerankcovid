
import SparseArrays
import JLD
import Plots

using Plots

include("webCrawler.jl")

# Adjacency matrix example taken from wikipedia: https://en.wikipedia.org/wiki/PageRank
adj_matrix = SparseArrays.sparse([
    0 0 0 0 1;
    0.5 0 0 0 0;
    0.5 0 0 0 0;
    0 1 0.5 0 0;
    0 0 0.5 1 0;
])

"""
    pagerank(adj_matrix, iterations, damp)

A simple implementation of the pagerank algorithm based on the number of
iterations. Returns ranking values.
"""
function pagerank(adj_matrix, iterations, damp)
    width = size(adj_matrix)[2]  # Width is incase it's not square
    r = ones(width) / width
    # See https://en.wikipedia.org/wiki/PageRank#Power_method
    iterator_matrix = (damp * adj_matrix .+ ((1 - damp) / width))
    # Run all the iterations. Preferred over a power since this is applied to a vector
    for i = 1:iterations
        println(i)
        r = iterator_matrix * r
    end
    return r
end

function normalizeMatrixRows(matrix)
    floats = convert(SparseMatrixCSC{Float64, Int64}, matrix)
    # go over the rows
    for i in 1:(size(floats, 1))
        total = sum(floats[i, :])
        if total > 0
            floats[i, :] /= total
        end
    end

    return floats
end

function performCrawl()
    println(now())
    beginCrawl(["https://www.algorithm-archive.org/"], "tmp/alg100.jld", 100)
    println(now())
    beginCrawl(["https://www.algorithm-archive.org/"], "tmp/alg1000.jld", 1000)
    println(now())
    beginCrawl(["https://nos.nl/"], "tmp/nos1000.jld", 1000)
    println(now())
    beginCrawl(["https://www.patisserieholtkamp.nl/"], "tmp/holtkamp1000.jld", 1000)
    println(now())
    beginCrawl(["https://www.wikipedia.org/"], "tmp/wiki1000.jld", 1000)
    println(now())
    beginCrawl(["https://mitchellfaas.com/"], "tmp/mitchell1000.jld", 1000)
    println(now())
    beginCrawl(["https://medium.com/"], "tmp/medium1000.jld", 1000)
    println(now())
end

function processInfo()
    for file in ["alg1000", "holtkamp1000", "mitchell1000",
                 "wiki1000",  "nos1000", "medium1000"]
        processFile(file)
    end
end

function processFile(file::String)
    dic = load(string("tmp/", file, ".jld"))
    webMatrix = dic["webMatrix"]
    normal = normalizeMatrixRows(webMatrix.matrix)
    prank = pagerank(transpose(normal), 100, 0.85)
    histogram(prank)
    png(string("figures/", file, ".png"))
end
