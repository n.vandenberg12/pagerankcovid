from scipy.sparse import lil_matrix  # noqa
import numpy as np


def structure_change(f):
    """Decorator with setup and teardown for structure changes"""
    def wrapped(self, *args, **kwargs):
        # Transform graph to one with fast structure changes
        self.graph = self.graph.tolil()
        # Apply function
        f(self, *args, **kwargs)
        # Transform back to one with fast vector multiplication (default)
        self.graph = self.graph.tocsr()

    return wrapped


class CovidSimulation:
    def __init__(self, size=500, alpha=1, recovery_rate=5e-3, death_rate=5e-3, infection_prob=5e-2):
        # Constant simulation data
        self.size = size
        self.death_rate = death_rate
        self.recovery_rate = recovery_rate
        self.infection_prob = infection_prob

        # Set an appropriate graph
        self.degrees = np.array(np.random.pareto(a=alpha, size=self.size), dtype=np.int16)
        self.graph = self._generate_random_graph()

        # Vectors representing node data
        self.dead = np.zeros(size, np.bool)
        self.recovered = np.zeros(size, np.bool)
        self.days_infected = np.zeros(size, np.int16)
        self.infectious = np.zeros(size, np.bool)
        self.indices = np.arange(size)

        # Set up datastructures needed to execute vacinations
        self.vaccination_order = None
        self.vaccination_idx = 0
        self.set_vaccination_order()

    def step(self):
        self.update_node_data()

        # Copy infectious to float representation
        float_infectious = np.array(self.infectious, dtype=np.float32)

        # Calculate probability of infection
        infection_probability = self.graph * float_infectious * self.infection_prob
        infection_probability[self.degrees != 0] /= self.degrees[self.degrees != 0]

        # Run all the stochastic simulations
        rand = np.random.rand(self.size)

        # Update infections
        self.infectious = self.infectious | (rand < infection_probability)

    @structure_change
    def update_node_data(self):
        # Update days infected
        self.days_infected[self.infectious] += 1

        self._update_dead()
        self._update_recovered()

        # Remove dead/recovered from is_infectious
        self.infectious[self.infectious & (self.dead | self.recovered)] = 0

    def _update_dead(self):
        # Note: The &, ~, | symbols are bitwise logical operators.
        mask = self.infectious & ~(self.dead | self.recovered)
        non_dead = self.dead[mask]
        non_dead[np.random.rand(len(non_dead)) < self.death_rate] = 1
        self.dead[mask] = non_dead

        for idx in self.indices[mask][non_dead]:
            self.delete_node(idx)

    def _update_recovered(self):
        # Note: The &, ~, | symbols are bitwise logical operators.
        mask = self.infectious & ~(self.dead | self.recovered)
        non_recovered = self.dead[mask]
        non_recovered[np.random.rand(len(non_recovered)) < self.death_rate] = 1
        self.dead[mask] = non_recovered

        for idx in self.indices[mask][non_recovered]:
            self.delete_node(idx)

    def _generate_random_graph(self):
        """Generates the random graph according to degrees calculated earlier"""
        graph = lil_matrix((self.size, self.size), dtype=bool)
        for i, degree in enumerate(self.degrees):
            degree = int(degree + 1)
            connections = np.random.choice(self.size - 1, degree)
            for j in connections:
                graph[i, j] = 1
                graph[j, i] = 1
        # lil is good for construction, but csr is good for arithmetic.
        return graph.tocsr()

    def _generate_random_infectious(self, n):
        """Generates a vector of appropriate size with random infections"""
        infectious = np.zeros(self.size, np.bool)
        for idx in np.random.choice(self.size - 1, n):
            infectious[idx] = 1
        return infectious

    def add_infectious(self, n):
        self.infectious[np.random.randint(0, self.size, n)] = 1

    def delete_node(self, idx):
        """Deletes a specified node from the graph. This function is assumed to be called by
        another, which already does lil/csr conversion."""
        self.graph[idx, :] = 0
        self.graph[:, idx] = 0

    def set_vaccination_order(self, strategy='highest_degree'):
        if strategy == 'highest_degree':
            self.vaccination_order = [(val, idx) for idx, val in enumerate(self.degrees)]
            self.vaccination_order.sort()
        else:
            raise ValueError(f'Strategy "{strategy}" is unkown.')

    @structure_change
    def vaccinate(self, n):
        """Performs n vacinations"""
        # Transform graph to one with fast structure changes
        self.graph = self.graph.tolil()
        for _ in range(n):
            if self.vaccination_idx >= self.size:
                break
            self.delete_node(self.vaccination_idx)
            self.vaccination_idx += 1


if __name__ == '__main__':
    a = CovidSimulation()
    a.add_infectious(5)
    sums = []
    for i in range(30):
        sums.append(a.infectious.sum())
        a.step()
    # Initiate lockdown
    a.infection_prob /= 10
    for i in range(120):
        sums.append(a.infectious.sum())
        a.step()
    # Open limited
    a.infection_prob *= 2
    for i in range(70):
        sums.append(a.infectious.sum())
        a.step()
    # Open fully
    a.infection_prob *= 5
    for i in range(70):
        sums.append(a.infectious.sum())
        a.step()

    import matplotlib.pyplot as plt
    plt.plot(sums)
    plt.show()
