"""
This file shows a minimal implementation which includes the following features:
    2.1: The basic simulation
    2.2: Deaths and Recoveries
    2.3: Sparse matrices and vectors (Memory Complexity)
"""

from scipy.sparse import lil_matrix  # noqa
import numpy as np


def structure_change(f):
    """Decorator with setup and teardown for structure changes"""
    def wrapped(self, *args, **kwargs):
        # Transform graph to one with fast structure changes
        self.graph = self.graph.tolil()
        # Apply function
        f(self, *args, **kwargs)
        # Transform back to one with fast vector multiplication (default)
        self.graph = self.graph.tocsr()

    return wrapped


class CovidSimulation:
    def __init__(self, size=500, alpha=1, recovery_rate=5e-3, death_rate=5e-3):
        # Constant simulation data
        self.size = size
        self.death_rate = death_rate
        self.recovery_rate = recovery_rate
        self.alpha = alpha  # Used in graph generation

        # Set an appropriate graph
        self.graph = self._generate_random_graph()

        # Vectors representing node data
        self.dead = np.zeros(size, np.bool)
        self.recovered = np.zeros(size, np.bool)
        self.infectious = np.zeros(size, np.bool)
        self.indices = np.arange(size)

    def step(self):
        self.update_node_data()
        self.infectious = self.graph * self.infectious

    @structure_change
    def update_node_data(self):
        # Update days infected
        self._update_dead()
        self._update_recovered()

        # Remove dead/recovered from is_infectious
        self.infectious[self.infectious & (self.dead | self.recovered)] = 0

    def _update_dead(self):
        # Note: The &, ~, | symbols are bitwise logical operators.
        updateable = self.infectious & ~(self.dead | self.recovered)

        non_dead = self.dead[updateable]
        non_dead[np.random.rand(len(non_dead)) < self.death_rate] = 1
        self.dead[updateable] = non_dead

        for idx in self.indices[updateable][non_dead]:
            self.delete_node(idx)

    def _update_recovered(self):
        # Note: The &, ~, | symbols are bitwise logical operators.
        updateable = self.infectious & ~(self.dead | self.recovered)

        non_recovered = self.dead[updateable]
        non_recovered[np.random.rand(len(non_recovered)) < self.death_rate] = 1
        self.recovered[updateable] = non_recovered

        for idx in self.indices[updateable][non_recovered]:
            self.delete_node(idx)

    def _generate_random_graph(self):
        """Generates the random graph according to degrees calculated earlier"""
        degrees = np.array(np.random.pareto(a=self.alpha, size=self.size), dtype=np.int16)
        graph = lil_matrix((self.size, self.size), dtype=bool)
        for i, degree in enumerate(degrees):
            degree = int(degree + 1)
            connections = np.random.choice(self.size - 1, degree)
            for j in connections:
                graph[i, j] = 1
                graph[j, i] = 1
        # lil is good for construction, but csr is good for arithmetic.
        return graph.tocsr()

    def _generate_random_infectious(self, n):
        """Generates a vector of appropriate size with random infections"""
        self.infectious = np.zeros(self.size, np.bool)
        self.add_infectious(n)

    def add_infectious(self, n):
        self.infectious[np.random.randint(0, self.size, n)] = 1

    def delete_node(self, idx):
        """Deletes a specified node from the graph. This function is assumed to be called by
        another, which already does lil/csr conversion."""
        self.graph[idx, :] = 0
        self.graph[:, idx] = 0


if __name__ == '__main__':
    a = CovidSimulation()
    a.add_infectious(5)
    sums = []
    for i in range(250):
        sums.append(a.infectious.sum())
        a.step()
        if i % 10 == 9:
            print(f'Step {i}:\tdead: {a.dead.sum()}, recovered:{a.recovered.sum()}')

    # Plotting infections
    import matplotlib.pyplot as plt
    plt.plot(sums)
    plt.show()
