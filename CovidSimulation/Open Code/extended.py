"""
This file shows a minimal implementation which includes the following features:
    2.1: The basic simulation
    2.2: Deaths and Recoveries
    2.3: Sparse matrices and vectors (Memory Complexity)
    2.4: Per-person infection rates
    2.6: Lockdown
Note that weighted edges are not used, but the infection logic used in weighted edges is
implemented for add-on 2.4. Because of this, the only change would be to actually weigh the edges
to represent standardized encounters
"""

from scipy.sparse import lil_matrix  # noqa
from tqdm import tqdm  # Progress bar for impatient people, like me.
import numpy as np



def structure_change(f):
    """Decorator with setup and teardown for structure changes"""
    def wrapped(self, *args, **kwargs):
        # Transform graph to one with fast structure changes
        self.graph = self.graph.tolil()
        # Apply function
        f(self, *args, **kwargs)
        # Transform back to one with fast vector multiplication (default)
        self.graph = self.graph.tocsr()

    return wrapped


class CovidSimulation:
    def __init__(self, size=10000, lockdown_factor=1/10):
        # Constant simulation data
        self.size = size
        self.death_rate = 5e-3
        self.recovery_rate = 5e-3
        # These are used for infection rates
        self.infection_rate = 0.05  # 5% chance of infection in a standardized encounter
        self.mask_effect = 0.8      # Masks are 80% effective
        self.vacc_effect = 0.95     # Vaccinations are 95% effective
        self.in_lockdown = False    # We start without lockdown
        self.lockdown_factor = 0.1  # In lockdown we assume 1/10th the amount of encounters
        self.dist_factor = 0.5      # Social distancing 1/2 the encounters
        self.social_distancing = 1

        # Set an appropriate graph
        self.degrees = np.array(np.random.pareto(a=1, size=self.size), dtype=np.int32)
        self.graph = self._generate_random_graph()

        # Vectors representing node data
        self.dead = np.zeros(size, np.bool)
        self.recovered = np.zeros(size, np.bool)
        self.is_infectious = np.zeros(size, np.bool)
        self.indices = np.arange(size)
        # These are used for infectious calculations
        self.wears_mask = np.random.rand(size)  # Percentage of time each person wears a mask
        self.is_vaccinated = np.zeros(size, np.bool)

        # Set up datastructures needed to execute vacinations
        self.vaccination_order = None
        self.vaccination_idx = 0
        self.set_vaccination_order()

    def step(self):
        self.update_node_data()

        # Copy is_infectious to float representation
        float_infectious = np.array(self.is_infectious, dtype=np.float32)

        # Calculate probability of infection
        std_encounters = (
            self.graph * float_infectious
            * (1 - self.mask_effect * self.wears_mask)         # Modify with mask effect
            * (1 - self.vacc_effect * self.is_vaccinated)      # Modify with vaccination effect
            * (not self.in_lockdown * self.lockdown_factor)    # Modify with lockdown effect
            * (1 - self.dist_factor * self.social_distancing)  # Modify with social distance factor
        )
        infect_prob = 1 - (1 - self.infection_rate) ** std_encounters

        # Run all the stochastic simulations
        rand = np.random.rand(self.size)

        # Update infections
        self.is_infectious = self.is_infectious | (rand < infect_prob)  # Did the node get infected?

    @structure_change
    def update_node_data(self):
        # Update days infected
        self._update_dead()
        self._update_recovered()

        # Remove dead/recovered from is_infectious
        self.is_infectious[self.is_infectious & (self.dead | self.recovered)] = 0

    def _update_dead(self):
        # Note: The &, ~, | symbols are bitwise logical operators.
        updateable = self.is_infectious & ~(self.dead | self.recovered)

        non_dead = self.dead[updateable]
        non_dead[np.random.rand(len(non_dead)) < self.death_rate] = 1
        self.dead[updateable] = non_dead

        for idx in self.indices[updateable][non_dead]:
            self.delete_node(idx)

    def _update_recovered(self):
        # Note: The &, ~, | symbols are bitwise logical operators.
        updateable = self.is_infectious & ~(self.dead | self.recovered)

        non_recovered = self.dead[updateable]
        non_recovered[np.random.rand(len(non_recovered)) < self.death_rate] = 1
        self.recovered[updateable] = non_recovered

        for idx in self.indices[updateable][non_recovered]:
            self.delete_node(idx)

    def _generate_random_graph(self):
        """Generates the random graph according to degrees calculated earlier"""
        graph = lil_matrix((self.size, self.size), dtype=bool)
        for i, degree in enumerate(self.degrees):
            degree = int(degree + 1)
            connections = np.random.choice(self.size - 1, degree)
            for j in connections:
                graph[i, j] = 1
                graph[j, i] = 1
        # lil is good for construction, but csr is good for arithmetic.
        return graph.tocsr()

    def _generate_random_infectious(self, n):
        """Generates a vector of appropriate size with random infections"""
        self.is_infectious = np.zeros(self.size, np.bool)
        self.add_infectious(n)

    def add_infectious(self, n):
        self.is_infectious[np.random.randint(0, self.size, n)] = 1

    def delete_node(self, idx):
        """Deletes a specified node from the graph. This function is assumed to be called by
        another, which already does lil/csr conversion."""
        self.graph[idx, :] = 0
        self.graph[:, idx] = 0

    # Simple lockdown functions
    def start_lockdown(self):
        self.in_lockdown = True

    def stop_lockdown(self):
        self.in_lockdown = False

    def set_vaccination_order(self, strategy='highest_degree'):
        if strategy == 'highest_degree':
            self.vaccination_order = [(val, idx) for idx, val in enumerate(self.degrees)]
            self.vaccination_order.sort()
        else:
            raise ValueError(f'Strategy "{strategy}" is unkown.')

    @structure_change
    def vaccinate(self, n):
        """Performs n vacinations"""
        # Transform graph to one with fast structure changes
        self.graph = self.graph.tolil()
        for _ in range(n):
            if self.vaccination_idx >= self.size:
                break
            self.delete_node(self.vaccination_idx)
            self.vaccination_idx += 1


def run_experiment(n=10000, ppe=False, social_distancing=False, vaccinations=False, lockdown=False,
                   days=250, times=3):

    infect = np.zeros(days)
    deaths = np.zeros(days)

    for _ in range(times):
        a = CovidSimulation(n)
        if not lockdown:
            a.lockdown_factor = 1
        if not social_distancing:
            a.social_distancing = 1
        if not ppe:
            a.mask_effect = 0

        lockdown_days_left = 0

        a.add_infectious(5)
        infectious_sums = np.zeros(days)
        deaths_sums = np.zeros(days)
        progress_bar = tqdm(range(days))
        for i in progress_bar:
            num_infectious = a.is_infectious.sum()
            infectious_sums[i] = num_infectious
            deaths_sums[i] = a.dead.sum()
            a.step()

            if lockdown:
                lockdown_days_left -= 1
                if  num_infectious > n / 10:
                    if lockdown_days_left <= 0:
                        a.start_lockdown()
                    lockdown_days_left = 30
                else:
                    if lockdown_days_left <= 0:
                        a.stop_lockdown()

            if vaccinations:
                a.vaccinate(n // 200)

            progress_bar.set_postfix({'dead': a.dead.sum(), 'recovered': a.recovered.sum()})

        infect += infectious_sums / times
        deaths += deaths_sums / times

    return infect, deaths

if __name__ == '__main__':
    n = 10000
    days = 365
    times = 5
    singular = False
    # p=ppe, s=social distancing, v=vaccinations, l=lockdown
    if singular:
        base_infs, base_deaths = run_experiment(n, False, False, False, False, days, times)
        p_infs, p_deaths = run_experiment(n, True, False, False, False, days, times)
        s_infs, s_deaths = run_experiment(n, False, True, False, False, days, times)
        v_infs, v_deaths = run_experiment(n, False, False, True, False, days, times)
        l_infs, l_deaths = run_experiment(n, False, False, False, True, days, times)
    else:
        base_infs, base_deaths = run_experiment(n, True, True, True, True, days, times)
        p_infs, p_deaths = run_experiment(n, False, True, True, True, days, times)
        s_infs, s_deaths = run_experiment(n, True, False, True, True, days, times)
        v_infs, v_deaths = run_experiment(n, True, True, False, True, days, times)
        l_infs, l_deaths = run_experiment(n, True, True, True, False, days, times)

    # Plotting infections
    import matplotlib.pyplot as plt

    prefix = '' if singular else 'no '

    plt.subplot(1, 2, 1)
    plt.title('Infectious')
    plt.plot(base_infs, label="baseline")
    plt.plot(p_infs, label=prefix+"ppe")
    plt.plot(s_infs, label=prefix+"social distancing")
    plt.plot(v_infs, label=prefix+"vaccinations")
    plt.plot(l_infs, label=prefix+"lockdown")
    plt.xlabel('Days')
    plt.ylabel('Number of nodes')
    plt.legend()

    plt.subplot(1, 2, 2)
    plt.title('Deaths')
    plt.plot(base_deaths, label="baseline")
    plt.plot(p_deaths, label=prefix+"ppe")
    plt.plot(s_deaths, label=prefix+"social distancing")
    plt.plot(v_deaths, label=prefix+"vaccinations")
    plt.plot(l_deaths, label=prefix+"lockdown")
    plt.xlabel('Days')
    plt.ylabel('Number of nodes')

    plt.tight_layout()
    plt.show()
