using SparseArrays

struct WebMatrix
    toIndex :: Dict{String, Int64}
    toURL :: Array{String}
    matrix :: SparseMatrixCSC{Int64, Int64}

    function WebMatrix(toIndex, toURL, matrix)
        return new(toIndex, toURL, matrix)
    end
end

# gives all the urls you can reach starting from a url
function to(webMatrix, url)
    if haskey(webMatrix.toIndex, url)
        index = webMatrix.toIndex[url]
        # construct the right vector
        vec = spzeros(size(webMatrix.matrix, 2), 1)
        vec[index] = 1
        connections = webMatrix.matrix * vec
        return map(x -> webMatrix.toURL[x], connections.rowval)
    else
        return []
    end
end

function from(webMatrix, url)
    if haskey(webMatrix.toIndex, url)
        index = webMatrix.toIndex[url]
        # construct the right vector
        vec = spzeros(size(webMatrix.matrix, 1), 1)
        vec[index] = 1
        connections = transpose(webMatrix.matrix) * vec
        return map(x -> webMatrix.toURL[x], connections.rowval)
    else
        return []
    end
end
